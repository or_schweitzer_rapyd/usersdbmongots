
export interface IService<T> 
{ 
    findByID(ID: string) : Promise<T>;
    findAll(): Promise<T[]>;
    getPage(pageNum : number, pageSize : number): Promise<T[]>
    save(item:T) : Promise<T>;
    deleteByID(ID: string) : Promise<T>;
    updateByID(ID: string,item: T) : Promise<T>;
}
    
    