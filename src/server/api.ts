// require('dotenv').config();
import express,{Express} from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./modules/user/user.controller.js";

import {
    errLogger,
    not_found,
} from "./middleware/errors.handler.js";

import {generateRequestID} from "./middleware/idGenerator.js";
import { logger } from "./middleware/logger.js";

const { PORT = 8080, HOST = "localhost", DB_URI } = process.env;


class MyApp{

    private app: Express| undefined;

    
    constructor() { 
        this.app = express();
       
        // middleware
        this.app.use(cors());
        this.app.use(morgan("dev"));

        // routing
        this.app.use(generateRequestID);
        this.app.use(logger);
        this.app.use("/api/users", user_router);

        //when no routes were matched...
        this.app.use("*", not_found);

        // final error handling
        this.app.use(errLogger);

        
    }
       
    //start the express api server
     start = async () =>{
        //connect to mongo db
        await connect_db(DB_URI as string);
        await ((this.app as Express).listen(Number(PORT), HOST as string));
        log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
    };
    
}

const myApp = new MyApp();
myApp.start().catch(console.log);
