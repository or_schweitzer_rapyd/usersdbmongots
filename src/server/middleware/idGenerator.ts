
import { Request,Response, NextFunction } from "express";
import * as utils from "../../utils.js";

export function generateRequestID(req : Request, res : Response,next : NextFunction)  {
    req.myID = utils.generateID();
    next();
}
