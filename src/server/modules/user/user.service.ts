import UserMongoService from "./user.service.Mongo.js";
import { IService } from "../../GlobalTypes.js";
import {User} from "./user.model.js";

const { DB_TYPE } = process.env;
let service : IService<User>;

const db_type_lower: string = (DB_TYPE as string).toLowerCase();
switch (db_type_lower) {
    case "mongo":
        service =  new UserMongoService();
        break;
    default:
        throw new Error("repository is not defined");
}

export default service;

   
