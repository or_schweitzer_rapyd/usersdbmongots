
import {IService} from "../../GlobalTypes.js";
import userModel,{User} from "./user.model.js";
export default class UserMongoService implements IService<User>{

   
    
    async findByID(ID: string): Promise<User> {
        const user = await userModel.findById(ID);
        
        return user;
    }

    async findAll(): Promise<User[]> {

        const users = await 
        userModel.find().select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);

       return users;
    }

    async getPage(pageNum :number ,pageSize : number) : Promise<User[]>{
        const users = await userModel
        .find()
        .skip(pageNum * pageSize)
        .limit(pageSize);

        return users;
    }

    async save(item: User): Promise<User> {
        const savedUser = await userModel.create(item);

        return (savedUser as unknown) as User;
    }


    async updateByID(ID:string,user:User) {
   
        const savedUser = await userModel.findByIdAndUpdate(
            ID,
           user,
            { new: true, upsert: false }
        );

        return (savedUser as unknown) as User;
    }


    async deleteByID(ID: string): Promise<User> {
        const removedUser = await userModel.findByIdAndRemove(ID);

        return (removedUser as unknown) as User;
    }
}

