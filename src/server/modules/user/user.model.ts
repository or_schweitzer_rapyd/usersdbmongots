import mongoose from "mongoose";
const { Schema, model } = mongoose;

const { PAGE_SIZE } = process.env;

export interface User {
    first_name: string,
    last_name: string,
    email: string,
    phone: string

}

export const UserSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    phone: String,
});
//UserSchema.
export default model("user", UserSchema);
